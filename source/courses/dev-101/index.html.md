---
layout: markdown_page
title: "DEV 101 - Contributing to Golang projects"
---

## Aim

This course is for people who have no previous experience with Golang,
but they know another programming language (for example Ruby or JavaScript)
and want to contribute to GitLab projects that are written in Golang.

## Steps

1. Install Go tools by
[following the official instructions](https://golang.org/doc/install)
1. Complete ["A Tour of Go"](https://tour.golang.org)
1. Join `#golang` and `#workhorse` Slack channels
1. Read a [critique of Golang](http://yager.io/programming/go.html)
to be aware of its limitations / weak sides
1. Take a look at [Golang subreddit](https://www.reddit.com/r/golang/) to get
a general feeling what the community is up to these days
1. Pick an issue from GitLab Workhorse issue tracker, preferably with
["Accepting Merge Requests" label](https://gitlab.com/gitlab-org/gitlab-workhorse/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Accepting%20Merge%20Requests)
