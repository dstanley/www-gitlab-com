---
layout: job_page
title: "Backend Lead"
---

This page has been deprecated and moved to the [Developer](/jobs/developer/) job description.
