---
layout: markdown_page
title: "Business Operations - Reporting"
---

## On this page
{:.no_toc}

- TOC
{:toc}




## Weekly marketing reporting

The Online Marketing team is in charge of updating reports and dashboards for the weekly marketing meetings. The current reporting that is being used can be found in [Google Drive](https://drive.google.com/open?id=0B1_ZzeTfG3XYMWc4U0RiRVcxcDA).

To access numbers for the reports, you can use the following links:
- [Web Traffic](https://analytics.google.com/analytics/web/#report/acquisition-channels/a37019925w65271535p67064032/)
- [Unique Visitors](https://analytics.google.com/analytics/web/#report/visitors-overview/a37019925w65271535p67064032/)
- [Respondents (leads)](https://app-ab13.marketo.com/#AR1358A1)
- [MQLs](https://app-ab13.marketo.com/#AR1365A1) (This is a little complicated as this report actually gives you lead creation date, so you need to go to `Smart List` -> `View Qualified Leads` then export and use a pivot table in Excel to get the actual numbers. I'm open to ideas if there's a better way to get the MQL numbers)
- [SQO #](https://na34.salesforce.com/00O61000003no6p)
- [SQO $ Value](https://na34.salesforce.com/00O61000003no6p)
- [Won #](https://na34.salesforce.com/00O61000003no6z)
- [Won $ Amount](https://na34.salesforce.com/00O61000003no6z)

## Reporting on large and up sales segment opportunity creation

An area of focus is the [large and strategic sales segments](/handbook/sales/#market-segmentation). For visibility into performance in these market segments we use a [Large and Up Opportunity Performance dashboard](https://na34.salesforce.com/01Z61000000J0h1) in salesforce.com. It tracks the measures [pending acceptance](https://about.gitlab.com/handbook/business-ops/customer-lifecycle/#demand-waterfall) opportunities, [SAO](https://about.gitlab.com/handbook/business-ops/customer-lifecycle/#demand-waterfall), and [SQO](https://about.gitlab.com/handbook/business-ops/customer-lifecycle/#demand-waterfall).

The dashboard analyzes opportunity measures by the dimensions `initial source type` [sales segmentation](/handbook/sales/#market-segmentation) and [sales qualification source](https://about.gitlab.com/handbook/sales/#tracking-sales-qualified-source-in-the-opportunity). We set our demand generation targets based on SAO count in large and up segments by `initial source type` and `sales qualification source` as follows:

- BDR Generated - opportunity is created by a BDR qualifying inbound demand. Sales qualification source is `BDR generated`.
- SDR Generated - opportunity is created by SDR prospecting. Sales qualification source is `SDR generated`.
- AE Generated, Marketing assisted - opportunity is created by an AE or SAL and is associated with a contact that has responded to one or more marketing programs. Sales qualification source is `AE generated` and `initial source` is not `AE generated`.
- AE Generated, Cold calling - opportunity is created by an AE or SAL but is not associated with any contacts that have responded to marketing programs. Sales qualification source is `AE generated` and `initial source` is `AE generated`.