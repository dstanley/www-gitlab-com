---
layout: markdown_page
title: "Social Marketing Handbook"
---

### Welcome to the Social Marketing Handbook!
{:.no_toc}

----

## On this page
{:.no_toc}

- TOC
{:toc}

----


## Social Channels and audience segmentation
 
### Twitter

#### Content/execution
* Broad audience - all our content gets shared here, but tone should still appeal to devs.
* 5 or more tweets per day, plus retweets 
  * Schedule between 7 am - 10 pm ET (to have some overlap with both EMEA and PT)
  * At least 30 mins apart
* Aim for variety & roughly even mix of main topics: Git, CI/CD, collaboration, DevOps, employer branding, product-specific, community articles & appreciation
* Check mentions at least once per day, pull out articles to share with the team and favorite positive mentions. 
*  Keep a list of our most engaged followers; keep these in mind for opportunities outside of social--blogging, live streamed interview etc.

#### Goals
* 15 tweets per day
* 2-4 videos published natively per month
* 5 lead-generating content items from the backlog published per month
* Consider [takeovers.](http://www.telegraph.co.uk/news/2017/07/12/work-experience-boy-takes-southern-rails-twitter-account-things/) Would need to develop an instructions kit & think about how/who to pilot.

### Facebook 
#### Content/execution
- Developer/community- and thought-leadership focused
- 1-2 facebook posts per day
- At least 2 hours apart
- Live events are more company/culture focused
    - talks by or AMAs with People Ops, etc.

#### Goals
- Consider streaming a webcast on Facebook Live (a simple toggle switch in Zoom allows this) and compare the performance to YouTube live streaming
- When the speaker cameras are enabled in Zoom webinars, natively publish clips of webcasts for post-promotion. 

### LinkedIn
#### Content/execution
- IT buyer/manager focus
- 3-5 posts per week
  - Non-tutorial original blog posts
  - Press releases/product announcements
- Posts around particular features, releases, company culture, people ops new practices, problem/solution or personal growth structured posts; include links back to homepage

#### Goals
* Identify key LinkedIn groups to join/share content in
* Native posts on LinkedIn
   * Encourage targeted employees to do so; think of incentive/framing as professional development that we will help edit. 
* Curate and share articles from other publishers on remote work/culture that we want to elevate

### Medium
#### Content/execution
* Developer/community- and thought leadership-focused
* Favorite articles about GitLab
* Aim to post one article to Medium every two weeks
* Cross-post our original thought-leadership posts to Medium, linking back to blog
  - Use the [import tool](https://help.medium.com/hc/en-us/articles/214550207-Import-post); do not copy and paste. 

### YouTube
#### Content/execution
- Developer/community-focused
- Live events are more tech focused
  - FGUs, pick your brain meetings,demos, brainstorms, kickoffs

#### Marketing process for streaming kickoffs & retros 
* create event in Zoom
* update the calendar event & google doc with the new meeting link
* update [image](https://docs.google.com/a/gitlab.com/presentation/d/1T1Rl5ptC81xkW4FSgJMSFExLrYN-FQArQlMeMwNkB_M/edit?usp=sharing) from Luke with the correct release number
* add speakers in the doc as panelists in Zoom; "promote" other team members who join to alternate hosts so that they can speak during the live stream

### Google+
#### Content/execution
- Re-post older articles from the archive periodically that we want to continue giving a boost
- Aim for at least one post per week

## Event Promotion
#### After Committing to an Event

- Find relevant handles (of speakers/moderators/hosts) or hashtags (if part of bigger conference for example), or create our own; try to include across all tweets for that event, characters permitting
- Find event-related images to attach to all tweets (event logo showing GitLab sponsorship; speaker team pic; GitLab logo; or any sharable content designed for that event)
- Schedule a tweet asap announcing our participation

#### Leading up to an Event, Schedule Tweets to go out:

- 1 week before event (counting down, include registration link if our event, include speaker team pic or other event branding where applicable)
- 1 day before event (same as above)
- During event referencing something in the content of the presentation if applicable/ not redundant (x2 or more depending on length of event and amount of content)
- Need to find balance between the dev advocates promoting themselves at events and when they are speaking- maybe one of these tweets is us retweeting them?

#### While at the event:
- Take pictures of booth and/or team members at booth
- Try to tweet them out ~1 hour or more before event (mention our location in the venue if applicable/useful)

#### After the event:
- Thank those who attended or stopped by our booth in tweet with photo from the event

## Ensuring your post will have a functional card and image

When you post a link on Facebook or Twitter, either you can
see only a link, or a full interactive card, which displays
information about that link: title, **description**, **image** and URL.

For Facebook these cards are configured via [OpenGraph Meta Tags][OG].
[Twitter Cards] were recently setup for our website as well.

Please compare the following images illustrating post's tweets.

A complete card will look like this:

![Twitter Card example - complete][twitter-card-comp]

An incomplete card will look like this:

![Twitter Card example - incomplete][twitter-card-incomp]

Note that the [first post] has a **specific description** and the image is a **screenshot**
of the post's cover image, taken from the [Blog landing page][blog]. This screenshot
can be taken locally when previewing the site at `localhost:4567/blog/`.

### Defining Social Media Sharing Information

Social Media Sharing info is set by the post or page frontmatter, by adding two variables:

```yaml
description: "short description of the post or page"
twitter_image: '/images/tweets/image.png'
```

This information is valid for the entire website, including all the webpages for about.GitLab.com, handbook, and blog posts.


#### Images

All the images or screenshots for `twitter_image` should be pushed to the [www-gitlab-com] project at `/source/images/tweets/` and must be named
after the page's file name.

For the [second post] above, note that the tweet image is the blog post cover image itself,
not the screenshot. Also, there's no `description` provided in the frontmatter, so our
Twitter Cards and Facebook's post will present the _fall back description_,
which is the same for all [about.GitLab.com].

For the handbook, make sure to name it so that it's obvious to which handbook it refers.
For example, for the Marketing Handbook, the image file name is `handbook-marketing.png`. For the Team Handbook, the image is called `handbook-gitlab.png`. For Support, it would be named `handbook-support.png`, and so on.

#### Description

The `description` meta tag [is important][description-tag]
for SEO, also is part of [Facebook Sharing][og] and [Twitter Cards]. We set it up in the
[post or page frontmatter](../blog/#frontmatter), as a small summary of what the post is about.

The description is not meant to repeat the post or page title, use your creativity to describe the content of the post or page.
Try to use about 70 to 100 chars in one sentence.

As soon as you add both description and social sharing image to a page or post, you must check and preview them with the [Twitter Card Validator]. You can also verify how it looks on the FB feed with the [Facebook Debugger].

#### Examples

To see it working, you can either share the page on Twitter or Facebook, or just test it with the [Twitter Card Validator].

- Complete post, with `description` and `twitter_image` defined:
[GitLab Master Plan](/2016/09/13/gitlab-master-plan/)
- Incomplete post, with only the `description` defined:
[Y Combinator Post](/2016/09/30/gitlabs-application-for-y-combinator-winter-2015/)
- Incomplete post, with none defined: [8.9 Release](/2016/06/22/gitlab-8-9-released/)
- Page with both defined: [Marketing Handbook](/handbook/marketing/)
- Page with only `twitter_image` defined: [Team Handbook](/handbook/)
- Page with none defined: [Blog landing page](/blog/)


<!-- Social Marketing Handbook - ref https://gitlab.com/gitlab-com/marketing/issues/524 -->

<!-- identifiers, in alphabetical order -->

[blog]: /blog/
[description-tag]: http://www.wordstream.com/meta-tags
[facebook debugger]: https://developers.facebook.com/tools/debug/
[first post]: /2016/07/19/markdown-kramdown-tips-and-tricks/
[OG]: https://developers.facebook.com/docs/sharing/webmasters#markup
[second post]: /2016/07/20/gitlab-is-open-core-github-is-closed-source/
[twitter card validator]: https://cards-dev.twitter.com/validator
[twitter cards]: https://dev.twitter.com/cards/overview
[twitter-card-comp]: /images/handbook/marketing/twitter-card-complete.jpg
[twitter-card-incomp]: /images/handbook/marketing/twitter-card-incomplete.jpg
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com
